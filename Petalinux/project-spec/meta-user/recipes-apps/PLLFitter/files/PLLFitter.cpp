#include <iostream>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

#define SI5326_ADDR 0x68 // I2C ADDRESS

using namespace std;

uint32_t best_n1_hs,
  best_nc_ls,
  best_n2_hs,
  best_n2_ls,
  best_n3n = 0;

int write_data(int fd, char * data, int len){
  int res = write(fd, data, len);
  if(res < len){
    return -1;
  }
  return 0;
}

int N31_finder(int mult_den, int mult_num, int input_clock) {

  int find_N2 = 0;
  int find_N13 = 0;
  int f3_candidate = 0;

  for(int in2h = 11; in2h >= 4; in2h--){
    for(int in1h = 11; in1h >= 4; in1h--){
      for(int in3 = 1; in3 < 500; in3++){
	for(int in2l = 2; in2l < 1000; in2l = in2l+2){
	  for(int incl = 1; incl < 1000; incl = incl+2){

	    find_N2 = in2l * in2h;
	    find_N13 = in3 * in1h * incl;
	    f3_candidate = input_clock / in3;
	    bool check = (double)find_N2/find_N13 == (double)mult_num/mult_den;
	    bool check_f3 = f3_candidate > 2
	      && f3_candidate < 2000;
	    bool check_fosc = f3_candidate * in2l * in2h > 4850000
	      && f3_candidate * in2l * in2h < 5670000;
	    
	    if(check){
	      if (check_f3){
		if (check_fosc){
		  best_n2_hs = in2h;
		  best_n2_ls = in2l;
		  best_n3n = in3;
		  best_n1_hs = in1h;
		  best_nc_ls = incl;
		  return 0;
		}
	      }
	    }

	    if (incl == 1) incl = 0;
	  }
	}
      }
    }
  }
}


int main (int argc, char** argv) {

  // CLOCKS
  int ck_in = 0;    //KHz [2 - 710,000]
  int f_3 = 0;      //KHz [2 - 2,000]
  double f_osc = 0; //MHz [4,850 - 5,670]
  double f_out = 2;    //KHz [2 - 1,475,000]
  
  // DIVIDERS
  int N1_HS = 0;    // [4 - 11]   n,n+1
  int NCn_LS = 0;   // [1 - 2^20] 2n,2(n+1) && 1
  int N2_HS = 11;    // [4 - 11]   n,n+1
  int N2_LS = 0;    // [2 - 2^20] 2n,2(n+1)
  int N2 = 1;
  int N3n = 1;      // [1 - 2^20] n,n+1

  if (argc !=4){
    cerr << "Usage: " << argv[0] << " <INPUT_FREQUENCY> <FREQ_MULTIPLICATION_NUMERATOR> <FREQ_MULTIPLICATION_DENOMINATOR>" << endl;
    return 1;
  }

  cout << endl << "_INPUT PARAMETERS________________________\n" << endl;

  cout << "Input Clock (in kHz). \nValues from 2 to 710,000 are accepted." << endl;
  ck_in = strtol(argv[1],NULL,10);
  cout << "Input clock: " << ck_in << " kHz" << endl;
  
  if (ck_in < 2 || ck_in > 710000){
    cout << "Unaccepted value. \nAccepted values ranges from 2 to 710,000 kHz" << endl;
    return 1;
  }

  cout << endl << "_OUTPUT PARAMETERS_______________________\n" << endl;

  int M_A = 0;
  int M_B = 0;
  cout << "Multiplication factor numerator (M = a/b)." << endl;
  M_A = strtol(argv[2],NULL,10);
  cout << "Multiplication factor denominator (M = a/b)." << endl;
  M_B = strtol(argv[3],NULL,10);
  cout << "Output Clock (in kHz). Values from 2 to 1,475,000 are accepted." << endl;
  f_out = ck_in * (double)M_A/M_B;
  cout << "Output clock: " << f_out << " khZ" << endl; 

  if (f_out < 2 || f_out > 1475000){
    cout << "Unaccepted value. Accepted values ranges from 2 to 1,475,000 kHz" << endl;
    return 1;
  }

  cout << endl << "_INTERNAL PARAMETERS_____________________\n" << endl;

  double M_factor = (double)f_out/ck_in;

  cout << "Multiplication factor=" << M_factor << endl;

  N31_finder(M_B, M_A, ck_in);

  cout << "F3: " << ck_in / best_n3n << " KHz" << endl;
  cout << "Fosc: " << (ck_in / best_n3n) * best_n2_hs * best_n2_ls << " KHz" << endl;
  cout << endl << "_FIT RESULTS_____________________________\n" << endl;
  cout << "N3N: " << best_n3n << endl;
  cout << "N2_HS: " << best_n2_hs << endl;
  cout << "N2_LS: " << best_n2_ls << endl;
  cout << "N1_HS: " << best_n1_hs << endl;
  cout << "NC_LS: " << best_nc_ls << endl;

  char c_n1_hs = (best_n1_hs - 4) << 5;

  char c_nc_ls [3];
  c_nc_ls[0] = (best_nc_ls - 1);
  c_nc_ls[1] = (best_nc_ls - 1) >> 8;
  c_nc_ls[2] = (best_nc_ls - 1) >> 16;

  char c_n2_hs = (best_n2_hs - 4) << 5;

  char c_n2_ls [3];
  c_n2_ls[0] = (best_n2_ls - 1);
  c_n2_ls[1] = (best_n2_ls - 1) >> 8;
  c_n2_ls[2] = (best_n2_ls - 1) >> 16;

  char c_n3n [3];
  c_n3n[0] = (best_n3n);
  c_n3n[1] = (best_n3n) >> 8;
  c_n3n[2] = (best_n3n) >> 16;


  // I2C COMMUNICATION

  cout << endl << "_INITIATING I2C COMMUNICATION____________\n" << endl;					     

  int fd;
  char devpath[] = "/dev/i2c-9";

  fd = open(devpath, O_RDWR);
  if (fd < 0){
    printf("Error opening device\n");
    return -1;
  }

  if (ioctl(fd, I2C_SLAVE, SI5326_ADDR) < 0){
    printf("Error selecting slave device\n");
    close(fd);
    return -1;
  }
  
  char si5326_regs[] = {
    0x00, 0x54, // free_run enabled, clk_out disables until calibration
    0x01, 0xE4, // 1st priority: clk1, 2nd priority: clk2
    0x02, 0x32, // Selects nominal f3dB bandwidth for PLL. (wut?)
    0x03, 0x15, // clkout disabled during calibration
    0x04, 0x92, // inout clk selection: Automatic Revertive
    0x05, 0xED, // set mos drive strenght: 32mA/8mA 
    0x06, 0x2D, // default value
    0x07, 0x2A, // default value
    0x08, 0x00, // default value
    0x09, 0xC0, // default value
    0x0A, 0x08, // clkout1 enabled
    0x0B, 0x40, // default value
    0x13, 0x29, // set trigger interval for phase monitor: 53 ms
    0x14, 0x36, // clk2 bad output: tristated
    0x15, 0xFE, // manual clock selection pin disabled
    0x16, 0xDF, // default value
    0x17, 0x1B, // LOS2_FLG ignored in generating interrupt output
    0x18, 0x3B, // FOS1_FLG ignored in generating interrupt output
    0x19, c_n1_hs, // N1_HS
    0x1F, c_nc_ls[2], // NC1_LS
    0x20, c_nc_ls[1], // NC1_LS
    0x21, c_nc_ls[0], // NC1_LS
    0x22, 0x00, // NC2_LS
    0x23, 0x00, // NC2_LS
    0x24, 0x01, // NC2_LS
    0x28, (c_n2_hs | c_n2_ls[2]), // N2_HS
    0x29, c_n2_ls[1], // N2_LS
    0x2A, c_n2_ls[0], // N2_LS
    0x2B, c_n3n[2], // N31
    0x2C, c_n3n[1], // N31
    0x2D, c_n3n[0], // N31
    0x2E, 0x00, // N32
    0x2F, 0x1B, // N32
    0x30, 0x09, // N32
    0x37, 0x00, // default value
    0x83, 0x1F, // default value
    0x84, 0x02, // default value
    0x89, 0x01, // !! not present !!
    0x8A, 0x0F, // default value
    0x8B, 0xFD, // Enable FOS monitoring
    0x8E, 0x00, // default value
    0x8F, 0x00, // default value
    0x88, 0x40  // command: start internal calibration
  };

  int res;
  for(int n=0; n < sizeof(si5326_regs)/2; n++){
    res = write_data(fd, si5326_regs + 2*n, 2);
    if (res < 0){
      printf("Error writing dev registers\n");
      break;
    }
  }

  close(fd);

  return 0;
}
