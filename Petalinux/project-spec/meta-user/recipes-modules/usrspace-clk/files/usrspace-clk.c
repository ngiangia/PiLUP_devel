#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/clk.h>

#include <linux/debugfs.h>
#include <linux/platform_device.h>

#define DEVICE_NAME "kin_clkgen"

// debugfs files
static struct dentry *usr_clk_fsdir;
static struct dentry *si570_bugfs;
// clock
static struct clk *kin_clkgen;
static u32 si570_rate;

static const struct of_device_id my_of_ids[] = {
	{ .compatible = "linux,userspace-clock-consumer" },
	{ }
};


/* ----------------------------------------------------
 * ------------ driver platform methods ---------------
 * ---------------------------------------------------- */
static int userspace_clk_probe(struct platform_device *pdev)
{
	kin_clkgen = clk_get(&pdev->dev, NULL);
	if (IS_ERR(kin_clkgen)){
		printk("usrclk: error getting clk\n");
		return PTR_ERR(kin_clkgen);
	}

	platform_set_drvdata(pdev, kin_clkgen);

	if (clk_prepare_enable(kin_clkgen) < 0){
		printk("usrclk: prepare clk failed\n");
		return -1;
	}

	clk_set_rate(kin_clkgen, 120240001UL);
	printk("usrclk: current clk rate: %li\n", clk_get_rate(kin_clkgen));

	return 0;
}

static int userspace_clk_remove(struct platform_device *pdev)
{  
	clk_disable_unprepare(kin_clkgen);
	clk_put(kin_clkgen);

	return 0;
}

// driver struct
static struct platform_driver userspace_clk = {
	.probe = userspace_clk_probe,
	.remove = userspace_clk_remove,
	.driver = {
		.name = "userspace-clock-consumer",
		.of_match_table = my_of_ids,
		.owner = THIS_MODULE,
	},
};

MODULE_DEVICE_TABLE(of, my_of_ids);

/* ----------------------------------------------------
 * ------------ file operations methods ---------------
 * ---------------------------------------------------- */

static int rate_u32_set(void *rate, u64 val)
{

	if ((val >= 10000000) && (val <= 900000000)){
		printk("usrclk: setting new rate: %llu\n", val);
		clk_set_rate(kin_clkgen, val+1UL);

		*(u32 *) rate = clk_get_rate(kin_clkgen);
		return 0;
	}

	printk("usrclk: invalid rate: %llu\n", val);
	return -EINVAL;
}

static int rate_u32_get(void *rate, u64 *val)
{
	*val = *(u32 *) rate;

	return 0;
}

DEFINE_SIMPLE_ATTRIBUTE(fops_u32, rate_u32_get, rate_u32_set, "%llu\n");


/* ----------------------------------------------------
 * -------------- init and exit methods ---------------
 * ---------------------------------------------------- */

static int __init kin_clk_init(void)
{ 
	usr_clk_fsdir = debugfs_create_dir("PiLUP_CLK", NULL);
	if (!usr_clk_fsdir){
		printk("usrclk: failed to create debugfs dir\n");
		return -ENOENT;
	}

	si570_bugfs = debugfs_create_file("si570_clk_rate", 0664, 
			usr_clk_fsdir, &si570_rate, &fops_u32);
	if (!si570_bugfs){
		printk("usrclk: failed to create si570 bugfs file\n");
		return -ENOENT;
	}

	platform_driver_register(&userspace_clk);

	return 0;
}


static void __exit kin_clk_exit(void)
{
	debugfs_remove_recursive(usr_clk_fsdir);
	platform_driver_unregister(&userspace_clk);
}

module_init(kin_clk_init);
module_exit(kin_clk_exit);

MODULE_AUTHOR("Me");
MODULE_DESCRIPTION("Quite bad clk generator userspace controller");
MODULE_LICENSE("GPL");
MODULE_VERSION("0.1");
