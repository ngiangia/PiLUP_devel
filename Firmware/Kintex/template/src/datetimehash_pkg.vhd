library ieee;
use ieee.std_logic_1164.all;

package datetimehash_pkg is
  -- Date information
  constant YEAR_INT  : integer                       := 2018;
  constant YEAR_HEX  : std_logic_vector(15 downto 0) := X"2018";
  constant MONTH_INT : integer                       := 06;
  constant MONTH_HEX : std_logic_vector(7 downto 0)  := X"06";
  constant DAY_INT   : integer                       := 20;
  constant DAY_HEX   : std_logic_vector(7 downto 0)  := X"20";
  constant DATE_HEX  : std_logic_vector(31 downto 0) := YEAR_HEX & MONTH_HEX & DAY_HEX;
  -- Time information
  constant HOUR_INT   : integer                       := 10;
  constant HOUR_HEX   : std_logic_vector(7 downto 0)  := X"10";
  constant MINUTE_INT : integer                       := 54;
  constant MINUTE_HEX : std_logic_vector(7 downto 0)  := X"54";
  constant SECOND_INT : integer                       := 12;
  constant SECOND_HEX : std_logic_vector(7 downto 0)  := X"12";
  constant TIME_HEX   : std_logic_vector(31 downto 0) := X"00" & HOUR_HEX & MINUTE_HEX & SECOND_HEX;
  -- Miscellaneous information
  constant EPOCH_INT  : integer := 1529506452;  -- Seconds since 1970-01-01_00:00:00
  -- git hash information 
  constant GIT_HASH_0 : std_logic_vector(31 downto 0)  := X"03be1f48";
  constant GIT_HASH_1 : std_logic_vector(31 downto 0)  := X"fd7dc92f";
  constant GIT_HASH_2 : std_logic_vector(31 downto 0)  := X"c304c622";
  constant GIT_HASH_3 : std_logic_vector(31 downto 0)  := X"d8729569";
  constant GIT_HASH_4 : std_logic_vector(31 downto 0)  := X"addf52ca";
end package;
