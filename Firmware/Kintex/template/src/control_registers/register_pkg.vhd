library ieee;
use ieee.std_logic_1164.all;

package register_pkg is
	subtype reg is std_logic_vector(31 downto 0);
	type reg_matrix is array (natural range <>) of reg;

	-- CONTROL registers
	constant N_CTRL_REGS							: integer	:= 1;
	
	constant reset_from_software_REG				: natural := 0;
	constant reset_from_software_RANGE				: natural := 0;


	-- STATUS registers
	constant N_STATUS_REGS							: integer	:= 7;
	
	constant DATE_HEX_REG							: natural := 0;
	type DATE_HEX_RANGE								is range 31 downto 0;
			
	constant TIME_HEX_REG							: natural := 1;
	type TIME_HEX_RANGE								is range 31 downto 0;
	
	constant GIT_HASH_0_REG							: natural := 2;
	type GIT_HASH_0_RANGE							is range 31 downto 0;
		
	constant GIT_HASH_1_REG							: natural := 3;
	type GIT_HASH_1_RANGE							is range 31 downto 0;
		
	constant GIT_HASH_2_REG							: natural := 4;
	type GIT_HASH_2_RANGE							is range 31 downto 0;
		
	constant GIT_HASH_3_REG							: natural := 5;
	type GIT_HASH_3_RANGE							is range 31 downto 0;	
	
	constant GIT_HASH_4_REG							: natural := 6;
	type GIT_HASH_4_RANGE							is range 31 downto 0;

	constant REGS_AXI_ADDR_WIDTH : integer := 31;

end package;
