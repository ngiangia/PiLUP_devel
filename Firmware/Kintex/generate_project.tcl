# SETUP project
puts "Please select Project to build. Currently supported: \"template\", \"Protocol_Converter\" and \"Data_Generator\""
set appl_name [gets stdin]
set prj_name "PiLUP_Kintex_${appl_name}"
set root_dir [file dirname [info script]]
set prj_dir "${root_dir}/${prj_name}"
create_project $prj_name $prj_dir -part xc7k325tffg900-2
set_property board_part xilinx.com:kc705:part0:1.5 [current_project]
set_property target_language VHDL [current_project]

# add sources
add_files -fileset constrs_1 "${root_dir}/${appl_name}/xdc/"
add_files -fileset sources_1 "${root_dir}/${appl_name}/src/"
update_compile_order -fileset sources_1

read_ip [glob "${root_dir}/${appl_name}/IP/*/*.xci"]
update_compile_order -fileset sources_1
generate_target all [get_ips]
#export_ip_user_files -of_objects [get_ips] -force -quiet

##################################################################################
############################# Create block design ################################
##################################################################################
create_bd_design -dir "${root_dir}/bd/${appl_name}" IP_blob

#################################  IP cells  ##################################

# !!match AXI_WUSER and ID_WIDTH with master!!
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_chip2chip axi_chip2chip_0
set_property -dict [list CONFIG.C_M_AXI_WUSER_WIDTH {0} CONFIG.C_M_AXI_ID_WIDTH {0} CONFIG.C_MASTER_FPGA {0}\
	CONFIG.C_USE_DIFF_IO {true} CONFIG.C_USE_DIFF_CLK {true}] [get_bd_cells axi_chip2chip_0]

create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_0
set_property -dict [list CONFIG.NUM_PORTS {4}] [get_bd_cells xlconcat_0]

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_iic axi_iic_0
set_property -dict [list CONFIG.IIC_BOARD_INTERFACE {iic_main}] [get_bd_cells axi_iic_0]

create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset proc_sys_reset_0

# !!match DCLK with axi CLK !!
create_bd_cell -type ip -vlnv xilinx.com:ip:xadc_wiz xadc_wiz_0
set_property -dict [list CONFIG.DCLK_FREQUENCY {200} CONFIG.OT_ALARM {false} CONFIG.USER_TEMP_ALARM {false} CONFIG.VCCINT_ALARM {false} \
	CONFIG.VCCAUX_ALARM {false} CONFIG.ADC_CONVERSION_RATE {1000}] [get_bd_cells xadc_wiz_0]


########################## Block Design ports #################################
# what's a for cycle?

# C2C
create_bd_port -dir I -type clk KZ_BUS_CLK_P
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_clk_in_p] [get_bd_ports KZ_BUS_CLK_P]
set_property CONFIG.FREQ_HZ 200000000 [get_bd_ports KZ_BUS_CLK_P]
create_bd_port -dir I -type clk KZ_BUS_CLK_N
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_clk_in_n] [get_bd_ports KZ_BUS_CLK_N]
set_property CONFIG.FREQ_HZ 200000000 [get_bd_ports KZ_BUS_CLK_N]
create_bd_port -dir O -type clk KZ_CLK_OUT_P
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_clk_out_p] [get_bd_ports KZ_CLK_OUT_P]
create_bd_port -dir O -type clk KZ_CLK_OUT_N
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_clk_out_n] [get_bd_ports KZ_CLK_OUT_N]
create_bd_port -dir I -from 8 -to 0 AXI_C2C_IN_P
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_data_in_p] [get_bd_ports AXI_C2C_IN_P]
create_bd_port -dir I -from 8 -to 0 AXI_C2C_IN_N
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_rx_diff_data_in_n] [get_bd_ports AXI_C2C_IN_N]
create_bd_port -dir O -from 8 -to 0 AXI_C2C_OUT_P
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_data_out_p] [get_bd_ports AXI_C2C_OUT_P]
create_bd_port -dir O -from 8 -to 0 AXI_C2C_OUT_N
connect_bd_net [get_bd_pins /axi_chip2chip_0/axi_c2c_selio_tx_diff_data_out_n] [get_bd_ports AXI_C2C_OUT_N]
# IIC
apply_bd_automation -rule xilinx.com:bd_rule:board -config {Board_Interface "iic_main ( IIC ) " }  [get_bd_intf_pins axi_iic_0/IIC]
# AXI clk
create_bd_port -dir I -type clk AXI_CLK200
set_property CONFIG.FREQ_HZ 200000000 [get_bd_ports AXI_CLK200]
# reset
create_bd_port -dir I -type rst ext_reset_in
connect_bd_net [get_bd_pins /proc_sys_reset_0/ext_reset_in] [get_bd_ports ext_reset_in]
# AXI interface for control registers (match ADDR_WIDTH with generic)
create_bd_intf_port -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_register_matrix
set_property -dict [list \
	CONFIG.ADDR_WIDTH {11} \
	CONFIG.PROTOCOL {AXI4LITE}] \
	[get_bd_intf_ports AXI_register_matrix]

######################## IP cells interconnections ############################

# connect axi interfaces to c2c with smartconnect
create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect smartconnect_0
set_property -dict [list CONFIG.NUM_MI {3} CONFIG.NUM_SI {1}] [get_bd_cells smartconnect_0]
connect_bd_intf_net [get_bd_intf_pins smartconnect_0/S00_AXI] [get_bd_intf_pins axi_chip2chip_0/m_axi]
connect_bd_intf_net [get_bd_intf_pins smartconnect_0/M00_AXI] [get_bd_intf_pins axi_iic_0/S_AXI]
connect_bd_intf_net [get_bd_intf_pins smartconnect_0/M01_AXI] [get_bd_intf_pins xadc_wiz_0/s_axi_lite]
connect_bd_intf_net [get_bd_intf_pins smartconnect_0/M02_AXI] [get_bd_intf_ports AXI_register_matrix]

# clock and reset "tree"
connect_bd_net [get_bd_pins [list \
	/AXI_CLK200 \
	/proc_sys_reset_0/slowest_sync_clk \
	/axi_chip2chip_0/idelay_ref_clk \
	[get_bd_pins */*aclk]]]
connect_bd_net [get_bd_pins [ list \
	/proc_sys_reset_0/peripheral_aresetn \
	[get_bd_pins  */*aresetn -filter {DIR == I}]]]

# intrs
connect_bd_net [get_bd_pins xlconcat_0/dout] [get_bd_pins axi_chip2chip_0/axi_c2c_s2m_intr_in]
connect_bd_net [get_bd_pins axi_iic_0/iic2intc_irpt] [get_bd_pins xlconcat_0/In0]
connect_bd_net [get_bd_pins xadc_wiz_0/ip2intc_irpt] [get_bd_pins xlconcat_0/In1]


##################### AXI ADDRESS assigments ################################
assign_bd_address [get_bd_addr_segs {axi_iic_0/S_AXI/Reg }]
assign_bd_address [get_bd_addr_segs {xadc_wiz_0/s_axi_lite/Reg }]
assign_bd_address [get_bd_addr_segs {AXI_register_matrix/Reg }]
set_property offset 0x7AA00000 [get_bd_addr_segs {axi_chip2chip_0/MAXI/SEG_axi_iic_0_Reg}]
set_property offset 0x7AA10000 [get_bd_addr_segs {axi_chip2chip_0/MAXI/SEG_xadc_wiz_0_Reg}]
set_property offset 0x7AA20000 [get_bd_addr_segs {axi_chip2chip_0/MAXI/SEG_AXI_register_matrix_Reg}]

regenerate_bd_layout
save_bd_design
update_compile_order -fileset sources_1
