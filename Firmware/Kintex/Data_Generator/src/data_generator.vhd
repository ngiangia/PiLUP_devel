----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/12/2017 05:28:22 PM
-- Design Name: 
-- Module Name: data_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.dataPackage.all;

entity data_generator is
Port (
	rst				: in std_logic;
	clk40			: in std_logic;
	clk156			: in std_logic;
	trig_in			: in std_logic;
	data_full		: in std_logic;
	data_out		: out slv64_array(3 downto 0);
	data_type_out	: out slv2_array(3 downto 0);
	data_valid		: out std_logic;
	trig_fifo_cnt	: out std_logic_vector(10 downto 0);
	n_hits			: in integer
);
end data_generator;

architecture Behavioral of data_generator is

--########################################################################
--##																	##
--##						Signals declaration							##
--##																	##
--########################################################################
	type state_type is (idle, process_trigger);  -- Define the states
	signal data_state						: state_type;
	
	signal hit_cnt							: integer := 0;
	signal trig_valid						: std_logic;
	signal trig_out							: std_logic_vector(19 downto 0);
	signal trig_fifo_empty, trig_fifo_re	: std_logic;
	signal BCID, L1ID						: std_logic_vector(9 downto 0):= (others => '0');
	signal data_random						: slv32_array(7 downto 0);
	signal data_random_valid				: std_logic_vector(7 downto 0);
	signal seed								: slv32_array(7 downto 0);
	signal load								: std_logic;
--########################################################################
--##																	##
--##						Component declaration						##
--##																	##
--########################################################################
					
	COMPONENT trigger_fifo
	PORT (
		rst : IN STD_LOGIC;
		wr_clk : IN STD_LOGIC;
		rd_clk : IN STD_LOGIC;
		din : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		wr_en : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
		valid : OUT STD_LOGIC;
		full : OUT STD_LOGIC;
		empty : OUT STD_LOGIC;
		rd_data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0)
	);
	END COMPONENT trigger_fifo;

	component updatedPRNG  
	GENERIC(
		width 		: integer := 32
	);
	PORT (
		clk			: IN STD_LOGIC; 
		reset		: IN STD_LOGIC; 
		load		: IN STD_LOGIC;
		step		: IN STD_LOGIC;
		seed		: IN STD_LOGIC_VECTOR(width-1 downto 0);
		dout		: OUT STD_LOGIC_VECTOR(width-1 downto 0);
		new_data	: OUT STD_LOGIC
	);
	END component updatedPRNG;

begin
	seed(0) <= x"12345678";
	seed(1) <= x"02345678";
	seed(2) <= x"12356789";
	seed(3) <= x"15678111";
--########################################################################
--##																	##
--##						Component implementation					##
--##																	##
--########################################################################

					
	trig_fifo_impl: trigger_fifo
	Port Map (
		rst				=> rst,
		wr_clk			=> clk40,
		rd_clk			=> clk156,
		din				=> L1ID & BCID,
		wr_en			=> trig_in,
		rd_en			=> trig_fifo_re,
		dout			=> trig_out,
		valid			=> trig_valid,
		full			=> open,
		empty			=> trig_fifo_empty,
		rd_data_count	=> trig_fifo_cnt
	);

	data_loop: for i in 0 to 7 generate
		data_generator_impl: updatedPRNG  
		Generic Map
		(
			width 		=> 32
		)
		Port map
		(
			clk			=> clk156,
			reset		=> rst, 
			load		=> load,
			step		=> '1',
			seed		=> seed(i),
			dout		=> data_random(i),
			new_data	=> data_random_valid(i)
		);
	
	
	end generate;

--########################################################################
--##																	##
--##						Processes implementation					##
--##																	##
--########################################################################

	fill_data_fifo: process(rst, clk156)
	begin
		if(rst = '1') then
			trig_fifo_re	<= '0';
			data_valid		<= '0';
			hit_cnt			<= 0;
			load			<= '1';
			data_state		<= idle;
		elsif(rising_edge(clk156)) then
			case data_state is
				when idle =>
					data_out(0)			<= x"1e00000000000000"; --idle
					data_type_out(0)	<= "10";
					data_out(1)			<= x"1e00000000000000"; --idle
					data_type_out(1)	<= "10";
					data_out(2)			<= x"1e00000000000000"; --idle
					data_type_out(2)	<= "10";
					data_out(3)			<= x"1e00000000000000"; --idle
					data_type_out(3)	<= "10";
					hit_cnt				<= 0;
					data_valid			<= '0';
					load				<= '0';
					trig_fifo_re		<= not data_full;
					if(trig_fifo_empty = '0') then
						trig_fifo_re		<= '0';
						data_valid			<= '1';
						data_out(0)			<= x"80000000000" & trig_out;
						data_type_out(0)	<= "10";
						case n_hits is
							when 0 =>
								data_out(1)			<= x"1e00000000000000"; --idle
								data_type_out(1)	<= "10";
								data_out(2)			<= x"1e00000000000000"; --idle
								data_type_out(2)	<= "10";
								data_out(3)			<= x"1e00000000000000"; --idle
								data_type_out(3)	<= "10";
							when 1 =>
								data_out(1)			<= x"1e040000" & '0' & data_random(2)(30 downto 0); 
								data_type_out(1)	<= "10";
								data_out(2)			<= x"1e00000000000000"; --idle
								data_type_out(2)	<= "10";
								data_out(3)			<= x"1e00000000000000"; --idle
								data_type_out(3)	<= "10";		 
							when 2 =>
								data_out(1)			<= x"1e040000" & '0' & data_random(2)(30 downto 0); 
								data_type_out(1)	<= "10";
								data_out(2)			<= x"1e040000" & '0' & data_random(4)(30 downto 0);
								data_type_out(2)	<= "10";
								data_out(3)			<= x"1e00000000000000"; --idle
								data_type_out(3)	<= "10";
							when 3 =>
								data_out(1)			<= x"1e040000" & '0' & data_random(2)(30 downto 0); 
								data_type_out(1)	<= "10";
								data_out(2)			<= x"1e040000" & '0' & data_random(4)(30 downto 0);
								data_type_out(2)	<= "10";
								data_out(3)			<= x"1e040000" & '0' & data_random(6)(30 downto 0);
								data_type_out(3)	<= "10";
							when others =>
								data_out(1)			<= x"1e040000" & '0' & data_random(2)(30 downto 0); 
								data_type_out(1)	<= "10";
								data_out(2)			<= x"1e040000" & '0' & data_random(4)(30 downto 0);
								data_type_out(2)	<= "10";
								data_out(3)			<= x"1e040000" & '0' & data_random(6)(30 downto 0);
								data_type_out(3)	<= "10";
								hit_cnt				<= 3;
								data_state		<= process_trigger;
						end case;
						--load			<= '1';
						
					end if;
				when process_trigger =>
					case (n_hits - hit_cnt) is
						when 0 =>
							data_out(0)			<= x"1e00000000000000"; --idle
							data_type_out(0)	<= "10";
							data_out(1)			<= x"1e00000000000000"; --idle
							data_type_out(1)	<= "10";
							data_out(2)			<= x"1e00000000000000"; --idle
							data_type_out(2)	<= "10";
							data_out(3)			<= x"1e00000000000000"; --idle
							data_type_out(3)	<= "10";
							data_state			<= idle;
						when 1 =>
							data_out(0)			<= x"1e040000" & '0' & data_random(0)(30 downto 0); 
							data_type_out(0)	<= "10";
							data_out(1)			<= x"1e00000000000000"; --idle
							data_type_out(1)	<= "10";
							data_out(2)			<= x"1e00000000000000"; --idle
							data_type_out(2)	<= "10";
							data_out(3)			<= x"1e00000000000000"; --idle
							data_type_out(3)	<= "10";
							data_state			<= idle;		 
						when 2 =>
							data_out(0)			<= '0' & data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= x"1e00000000000000"; --idle
							data_type_out(1)	<= "10";
							data_out(2)			<= x"1e00000000000000"; --idle
							data_type_out(2)	<= "10";
							data_out(3)			<= x"1e00000000000000"; --idle
							data_type_out(3)	<= "10";
							data_state			<= idle; 
						when 3 =>
							data_out(0)			<= '0' & data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= x"1e040000" & '0' & data_random(2)(30 downto 0);  
							data_type_out(1)	<= "10";
							data_out(2)			<= x"1e00000000000000"; --idle
							data_type_out(2)	<= "10";
							data_out(3)			<= x"1e00000000000000"; --idle
							data_type_out(3)	<= "10";
							data_state			<= idle;		 
						when 4 =>
							data_out(0)			<= '0' & data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= '0' & data_random(2)(30 downto 0) & '0' & data_random(3)(30 downto 0); 
							data_type_out(1)	<= "01";
							data_out(2)			<= x"1e00000000000000"; --idle
							data_type_out(2)	<= "10";
							data_out(3)			<= x"1e00000000000000"; --idle
							data_type_out(3)	<= "10";
							data_state			<= idle;	   
						when 5 =>
							data_out(0)			<= '0' & data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= '0' & data_random(2)(30 downto 0) & '0' & data_random(3)(30 downto 0); 
							data_type_out(1)	<= "01";
							data_out(2)			<= x"1e040000" & '0' & data_random(4)(30 downto 0); 
							data_type_out(2)	<= "10";
							data_out(3)			<= x"1e00000000000000"; --idle
							data_type_out(3)	<= "10";
							data_state			<= idle;
						when 6 =>
							data_out(0)			<= '0' & data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= '0' & data_random(2)(30 downto 0) & '0' & data_random(3)(30 downto 0); 
							data_type_out(1)	<= "01";
							data_out(2)			<='0' &  data_random(4)(30 downto 0) & '0' & data_random(5)(30 downto 0); 
							data_type_out(2)	<= "01";
							data_out(3)			<= x"1e00000000000000"; --idle
							data_type_out(3)	<= "10";
							data_state			<= idle;
						when 7 =>
							data_out(0)			<= '0' & data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= '0' & data_random(2)(30 downto 0) & '0' & data_random(3)(30 downto 0); 
							data_type_out(1)	<= "01";
							data_out(2)			<= '0' & data_random(4)(30 downto 0) & '0' & data_random(5)(30 downto 0); 
							data_type_out(2)	<= "01";
							data_out(3)			<= x"1e040000" & '0' & data_random(6)(30 downto 0); 
							data_type_out(3)	<= "10";
							data_state			<= idle;
						when 8 =>
							data_out(0)			<='0' &  data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= '0' & data_random(2)(30 downto 0) & '0' & data_random(3)(30 downto 0); 
							data_type_out(1)	<= "01";
							data_out(2)			<= '0' & data_random(4)(30 downto 0) & '0' & data_random(5)(30 downto 0); 
							data_type_out(2)	<= "01";
							data_out(3)			<= '0' & data_random(6)(30 downto 0) & '0' & data_random(7)(30 downto 0);
							data_type_out(3)	<= "01";
							data_state			<= idle;	
						when others =>
							data_out(0)			<= '0' & data_random(0)(30 downto 0) & '0' & data_random(1)(30 downto 0); 
							data_type_out(0)	<= "01";
							data_out(1)			<= '0' & data_random(2)(30 downto 0) & '0' & data_random(3)(30 downto 0); 
							data_type_out(1)	<= "01";
							data_out(2)			<= '0' & data_random(4)(30 downto 0) & '0' & data_random(5)(30 downto 0); 
							data_type_out(2)	<= "01";
							data_out(3)			<= '0' & data_random(6)(30 downto 0) & '0' & data_random(7)(30 downto 0);
							data_type_out(3)	<= "01";
							hit_cnt				<= hit_cnt + 8;
					end case;
			end case;
		end if;
	end process fill_data_fifo;


end Behavioral;
