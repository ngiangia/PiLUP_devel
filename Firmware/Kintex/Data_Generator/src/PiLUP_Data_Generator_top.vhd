----------------------------------------------------------------------------------
-- Company: INFN & University of Bologna
-- Developer: Nico Giangiacomi
--
-- Create Date: 02/13/2018 03:58:37 PM
-- Design Name: PiLUP_Protocol_Converter
-- Module Name: PiLUP_Protocol_Converter_top - Behavioral
-- Project Name: PiLUP_Protocol_Converter
-- Target Devices: Bologna INFN PiLUP Board
-- Tool Versions: 1.0
-- Description: Protocol converter to interface FE chip RD53A to readout board Felix
--
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.dataPackage.all;
use work.register_pkg.all;
use work.datetimehash_pkg.all;

entity PiLUP_Data_Generator is
Port (

	--  CLOCK SIGNALS
	SYSCLK_P 		: in std_logic;
	SYSCLK_N 		: in std_logic;
	
	-- IP BLOB external interfaces (C2C + i2c)
	AXI_C2C_IN_N : in std_logic_vector ( 8 downto 0 );
	AXI_C2C_IN_P : in std_logic_vector ( 8 downto 0 );
	AXI_C2C_OUT_N : out std_logic_vector ( 8 downto 0 );
	AXI_C2C_OUT_P : out std_logic_vector ( 8 downto 0 );
	KZ_BUS_CLK_N : in std_logic;
	KZ_BUS_CLK_P : in std_logic;
	KZ_CLK_OUT_N : out std_logic;
	KZ_CLK_OUT_P : out std_logic;
	ext_reset_in : in std_logic;
	iic_main_scl_io : inout std_logic;
	iic_main_sda_io : inout std_logic
	
);
end PiLUP_Data_Generator;

architecture Behavioral of PiLUP_Data_Generator is
	----------------------------------------------------------
	--                                                      --
	--                  Signal Declaration                  --
	--                                                      --
	----------------------------------------------------------
	signal status : reg_matrix(0 to N_STATUS_REGS-1):= (others => (others => '0'));
	signal ctrl : reg_matrix(0 to N_CTRL_REGS-1);

	-- Axi interface signals
	signal AXI_reg_matrix_araddr   : std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
	signal AXI_reg_matrix_arprot   : std_logic_vector (2 downto 0);
	signal AXI_reg_matrix_arready  : std_logic;
	signal AXI_reg_matrix_arvalid  : std_logic;
	signal AXI_reg_matrix_awaddr   : std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
	signal AXI_reg_matrix_awprot   : std_logic_vector (2 downto 0);
	signal AXI_reg_matrix_awready  : std_logic;
	signal AXI_reg_matrix_awvalid  : std_logic;
	signal AXI_reg_matrix_bready   : std_logic;
	signal AXI_reg_matrix_bresp    : std_logic_vector (1 downto 0);
	signal AXI_reg_matrix_bvalid   : std_logic;
	signal AXI_reg_matrix_rdata    : std_logic_vector (31 downto 0);
	signal AXI_reg_matrix_rready   : std_logic;
	signal AXI_reg_matrix_rresp    : std_logic_vector (1 downto 0);
	signal AXI_reg_matrix_rvalid   : std_logic;
	signal AXI_reg_matrix_wdata    : std_logic_vector (31 downto 0);
	signal AXI_reg_matrix_wready   : std_logic;
	signal AXI_reg_matrix_wstrb    : std_logic_vector (3 downto 0);
	signal AXI_reg_matrix_wvalid   : std_logic;
	
	
	signal iic_main_scl_i : std_logic;
	signal iic_main_scl_o : std_logic;
	signal iic_main_scl_t : std_logic;
	signal iic_main_sda_i : std_logic;
	signal iic_main_sda_o : std_logic;
	signal iic_main_sda_t : std_logic;

	
	--clock signals
	signal sysclk, sysclk40, sysclk156        	: std_logic;
	signal rst, enable							: std_logic;
	signal trig_int, n_hits						: std_logic_vector(7 downto 0):=x"00";
	signal trig_i								: std_logic;
	
	signal n_hits_integer						: integer; 
	signal data_out_i							: slv64_array(3 downto 0);
	signal data_type_i							: slv2_array(3 downto 0);
	signal data_valid_i							: std_logic;
	signal fifo_trig_cnt_i						: std_logic_vector(10 downto 0);
	
	signal cnt_tot, cnt_hit						: slv64_array(3 downto 0);
	
	----------------------------------------------------------
	--                                                      --
	--                 Component Declaration                --
	--                                                      --
	----------------------------------------------------------
	component IP_blob is
	generic(
		C_S_AXI_ADDR_WIDTH : integer := REGS_AXI_ADDR_WIDTH
	);
	port (
		iic_main_scl_i : in std_logic;
		iic_main_scl_o : out std_logic;
		iic_main_scl_t : out std_logic;
		iic_main_sda_i : in std_logic;
		iic_main_sda_o : out std_logic;
		iic_main_sda_t : out std_logic;
		KZ_BUS_CLK_P : in std_logic;
		KZ_BUS_CLK_N : in std_logic;
		KZ_CLK_OUT_P : out std_logic;
		KZ_CLK_OUT_N : out std_logic;
		AXI_C2C_IN_P : in std_logic_vector(8 downto 0);
		AXI_C2C_IN_N : in std_logic_vector(8 downto 0);
		AXI_C2C_OUT_P : out std_logic_vector(8 downto 0);
		AXI_C2C_OUT_N : out std_logic_vector(8 downto 0);
		AXI_CLK200  :  in std_logic;
		AXI_register_matrix_araddr : out std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
		AXI_register_matrix_arprot : out std_logic_vector (2 downto 0);
		AXI_register_matrix_arready : in std_logic;
		AXI_register_matrix_arvalid : out std_logic;
		AXI_register_matrix_awaddr : out std_logic_vector (REGS_AXI_ADDR_WIDTH-1 downto 0);
		AXI_register_matrix_awprot : out std_logic_vector (2 downto 0);
		AXI_register_matrix_awready : in std_logic;
		AXI_register_matrix_awvalid : out std_logic;
		AXI_register_matrix_bready : out std_logic;
		AXI_register_matrix_bresp : in std_logic_vector (1 downto 0);
		AXI_register_matrix_bvalid : in std_logic;
		AXI_register_matrix_rdata : in std_logic_vector (31 downto 0);
		AXI_register_matrix_rready : out std_logic;
		AXI_register_matrix_rresp : in std_logic_vector (1 downto 0);
		AXI_register_matrix_rvalid : in std_logic;
		AXI_register_matrix_wdata : out std_logic_vector (31 downto 0);
		AXI_register_matrix_wready : in std_logic;
		AXI_register_matrix_wstrb : out std_logic_vector (3 downto 0);
		AXI_register_matrix_wvalid : out std_logic;
		ext_reset_in : in std_logic
	);
	end component IP_blob;

	component IOBUF is
	port (
		I : in std_logic;
		O : out std_logic;
		T : in std_logic;
		IO : inout std_logic
	);
	end component IOBUF;
	
	component sysclk_wiz
	port
	(
		-- Clock in ports
		-- Clock out ports
		clk200_out          : out    std_logic;
		clk40_out          : out    std_logic;
		clk156_out          : out    std_logic;
		-- Status and control signals
		reset             : in     std_logic;
		locked            : out    std_logic;
		clk_in1_p         : in     std_logic;
		clk_in1_n         : in     std_logic
	);
	end component;
	
	COMPONENT main_ila
	PORT (
		clk : IN STD_LOGIC;
		probe0 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe1 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
		probe2 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe3 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
		probe4 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe5 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
		probe6 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe7 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
		probe8 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe9 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe10 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe11 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe12 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe13 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe14 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
		probe15 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
		probe16 : IN STD_LOGIC_VECTOR(10 DOWNTO 0)
	);
	END COMPONENT  ;
	
begin

	n_hits_integer <= to_integer(unsigned(n_hits));

	----------------------------------------------------------
	--                                                      --
	--                Component Implementation              --
	--                                                      --
	----------------------------------------------------------
	register_block: entity work.axi_regs_test
	generic map(
		C_S_AXI_ADDR_WIDTH => REGS_AXI_ADDR_WIDTH
	)
	port map(
		S_AXI_ACLK	 	=> sysclk,
		S_AXI_ARESETN	=> ext_reset_in,
		S_AXI_AWADDR 	=> AXI_reg_matrix_awaddr,
		S_AXI_AWPROT 	=> AXI_reg_matrix_awprot,
		S_AXI_AWVALID	=> AXI_reg_matrix_awvalid,
		S_AXI_AWREADY	=> AXI_reg_matrix_awready,
		S_AXI_WDATA	 	=> AXI_reg_matrix_wdata,
		S_AXI_WSTRB	 	=> AXI_reg_matrix_wstrb,
		S_AXI_WVALID 	=> AXI_reg_matrix_wvalid,
		S_AXI_WREADY 	=> AXI_reg_matrix_wready,
		S_AXI_BRESP	 	=> AXI_reg_matrix_bresp,
		S_AXI_BVALID 	=> AXI_reg_matrix_bvalid,
		S_AXI_BREADY 	=> AXI_reg_matrix_bready,
		S_AXI_ARADDR 	=> AXI_reg_matrix_araddr,
		S_AXI_ARPROT 	=> AXI_reg_matrix_arprot,
		S_AXI_ARVALID	=> AXI_reg_matrix_arvalid,
		S_AXI_ARREADY	=> AXI_reg_matrix_arready,
		S_AXI_RDATA	 	=> AXI_reg_matrix_rdata,
		S_AXI_RRESP	 	=> AXI_reg_matrix_rresp,
		S_AXI_RVALID 	=> AXI_reg_matrix_rvalid,
		S_AXI_RREADY 	=> AXI_reg_matrix_rready,

		STATUS => status,
		CTRL => ctrl
	);

	IP_block_i: component IP_blob
	port map (
		AXI_C2C_IN_N => AXI_C2C_IN_N,
		AXI_C2C_IN_P => AXI_C2C_IN_P,
		AXI_C2C_OUT_N => AXI_C2C_OUT_N,
		AXI_C2C_OUT_P => AXI_C2C_OUT_P,
		KZ_BUS_CLK_N => KZ_BUS_CLK_N,
		KZ_BUS_CLK_P => KZ_BUS_CLK_P,
		KZ_CLK_OUT_N => KZ_CLK_OUT_N,
		KZ_CLK_OUT_P => KZ_CLK_OUT_P,
		ext_reset_in => ext_reset_in,
		iic_main_scl_i => iic_main_scl_i,
		iic_main_scl_o => iic_main_scl_o,
		iic_main_scl_t => iic_main_scl_t,
		iic_main_sda_i => iic_main_sda_i,
		iic_main_sda_o => iic_main_sda_o,
		iic_main_sda_t => iic_main_sda_t,
		AXI_CLK200 => sysclk,

		AXI_register_matrix_araddr  => AXI_reg_matrix_araddr,
		AXI_register_matrix_arprot  => AXI_reg_matrix_arprot,
		AXI_register_matrix_arready => AXI_reg_matrix_arready,
		AXI_register_matrix_arvalid => AXI_reg_matrix_arvalid,
		AXI_register_matrix_awaddr  => AXI_reg_matrix_awaddr,
		AXI_register_matrix_awprot  => AXI_reg_matrix_awprot,
		AXI_register_matrix_awready => AXI_reg_matrix_awready,
		AXI_register_matrix_awvalid => AXI_reg_matrix_awvalid,
		AXI_register_matrix_bready  => AXI_reg_matrix_bready,
		AXI_register_matrix_bresp   => AXI_reg_matrix_bresp,
		AXI_register_matrix_bvalid  => AXI_reg_matrix_bvalid,
		AXI_register_matrix_rdata   => AXI_reg_matrix_rdata,
		AXI_register_matrix_rready  => AXI_reg_matrix_rready,
		AXI_register_matrix_rresp   => AXI_reg_matrix_rresp,
		AXI_register_matrix_rvalid  => AXI_reg_matrix_rvalid,
		AXI_register_matrix_wdata   => AXI_reg_matrix_wdata,
		AXI_register_matrix_wready  => AXI_reg_matrix_wready,
		AXI_register_matrix_wstrb   => AXI_reg_matrix_wstrb,
		AXI_register_matrix_wvalid  => AXI_reg_matrix_wvalid
	);

	iic_main_scl_iobuf: component IOBUF
	port map(
		I => iic_main_scl_o,
		IO => iic_main_scl_io,
		O => iic_main_scl_i,
		T => iic_main_scl_t
	);
	iic_main_sda_iobuf: component IOBUF
	port map(
		I => iic_main_sda_o,
		IO => iic_main_sda_io,
		O => iic_main_sda_i,
		T => iic_main_sda_t
	);
	
	--                                            register connection
	--               control registers
	
	register_sync_156: process (sysclk156)
	begin
		if(rising_edge(sysclk156)) then
			rst													<= ctrl(reset_from_software_REG)(reset_from_software_RANGE);
		end if;
	end process register_sync_156;
		
	
	register_sync_40: process (sysclk40)
	begin
		if(rising_edge(sysclk40)) then
			enable												<= ctrl(enable_REG)(enable_RANGE);
		end if;
	end process register_sync_40;
	
	--                status registers
	register_status_sync: process (sysclk)
		begin
			if(rising_edge(sysclk)) then
				status(DATE_HEX_REG)(DATE_HEX_RANGE)			<= DATE_HEX;
				status(TIME_HEX_REG)(TIME_HEX_RANGE)			<= TIME_HEX;
				status(GIT_HASH_0_REG)(GIT_HASH_0_RANGE)		<= GIT_HASH_0;
				status(GIT_HASH_1_REG)(GIT_HASH_1_RANGE)		<= GIT_HASH_1;
				status(GIT_HASH_2_REG)(GIT_HASH_2_RANGE)		<= GIT_HASH_2;
				status(GIT_HASH_3_REG)(GIT_HASH_3_RANGE)		<= GIT_HASH_3;
				status(GIT_HASH_4_REG)(GIT_HASH_4_RANGE)		<= GIT_HASH_4;
				status(cnt_tot_0_msb_REG)(cnt_tot_0_msb_RANGE)	<= cnt_tot(0)(63 downto 32);
				status(cnt_tot_0_lsb_REG)(cnt_tot_0_lsb_RANGE)	<= cnt_tot(0)(31 downto 0);	
				status(cnt_tot_1_msb_REG)(cnt_tot_1_msb_RANGE)	<= cnt_tot(1)(63 downto 32);
				status(cnt_tot_1_lsb_REG)(cnt_tot_1_lsb_RANGE)	<= cnt_tot(1)(31 downto 0);	
				status(cnt_tot_2_msb_REG)(cnt_tot_2_msb_RANGE)	<= cnt_tot(2)(63 downto 32);
				status(cnt_tot_2_lsb_REG)(cnt_tot_2_lsb_RANGE)	<= cnt_tot(2)(31 downto 0);
				status(cnt_tot_3_msb_REG)(cnt_tot_3_msb_RANGE)	<= cnt_tot(3)(63 downto 32);
				status(cnt_tot_3_lsb_REG)(cnt_tot_3_lsb_RANGE)	<= cnt_tot(3)(31 downto 0);
				status(cnt_hit_0_msb_REG)(cnt_hit_0_msb_RANGE)	<= cnt_hit(0)(63 downto 32);
				status(cnt_hit_0_lsb_REG)(cnt_hit_0_lsb_RANGE)	<= cnt_hit(0)(31 downto 0);
				status(cnt_hit_1_msb_REG)(cnt_hit_1_msb_RANGE)	<= cnt_hit(1)(63 downto 32);
				status(cnt_hit_1_lsb_REG)(cnt_hit_1_lsb_RANGE)	<= cnt_hit(1)(31 downto 0);
				status(cnt_hit_2_msb_REG)(cnt_hit_2_msb_RANGE)	<= cnt_hit(2)(63 downto 32);
				status(cnt_hit_2_lsb_REG)(cnt_hit_2_lsb_RANGE)	<= cnt_hit(2)(31 downto 0);
				status(cnt_hit_3_msb_REG)(cnt_hit_3_msb_RANGE)	<= cnt_hit(3)(63 downto 32);
				status(cnt_hit_3_lsb_REG)(cnt_hit_3_lsb_RANGE)	<= cnt_hit(3)(31 downto 0);
				status(fifo_trig_cnt_REG)(fifo_trig_cnt_RANGE)	<= fifo_trig_cnt_i;
			end if;
		end process register_status_sync;
	--						USER LOGIC					--
	
	

	main_ila_impl: main_ila
	Port Map (
		clk 		=>	sysclk156,	
		probe0		=>  data_out_i(0),
		probe1		=>	data_type_i(0),
		probe2		=>	data_out_i(1),
		probe3		=>	data_type_i(1),
		probe4		=>	data_out_i(2),
		probe5		=>	data_type_i(2),
		probe6		=>	data_out_i(3),
		probe7		=>	data_type_i(3),
		probe8		=>	cnt_tot (0),
		probe9		=>	cnt_hit (0),
		probe10		=>	cnt_tot (1),
		probe11		=>	cnt_hit (1),
		probe12		=>	cnt_tot (2),
		probe13		=>	cnt_hit (2),
		probe14		=>	cnt_tot (3),
		probe15		=>	cnt_hit (3),
		probe16		=>	fifo_trig_cnt_i
	);
	
		
		
		
	sysclock_wizard_impl: sysclk_wiz
	Port Map
	(
		clk200_out		=> sysclk,
		clk40_out		=> sysclk40,
		clk156_out		=> sysclk156,
		reset			=> '0',
		locked			=> open,
		clk_in1_p		=> SYSCLK_P,
		clk_in1_n		=> SYSCLK_N
	);
	
	Trig_gen_impl: entity work.TTC_generator
	Port Map( 
		clk40			=> sysclk40,
		rst				=> not enable,
		trig_interval	=> trig_int,
		trig_out		=> trig_i
	);
	
	data_generator_impl: entity work.data_generator
	Port Map (
		rst				=> rst,
		clk40			=> sysclk40,
		clk156			=> sysclk156,
		trig_in			=> trig_i,
		data_full		=> '0',
		data_out		=> data_out_i,
		data_type_out	=> data_type_i,
		data_valid		=> data_valid_i,
		trig_fifo_cnt	=> fifo_trig_cnt_i,
		n_hits			=> n_hits_integer
	);
	
	----------------------------------------------------------
	--                                                      --
	--                 Process Implementation               --
	--                                                      --
	----------------------------------------------------------
	
	occupancy_cnt: process (rst, sysclk156)
	begin
		if(rst = '1') then
			for i in 0 to 3 loop
				cnt_tot (i)			<= (others => '0');
				cnt_hit	(i)			<= (others => '0');
			end loop;
		elsif(rising_edge(sysclk156)) then
			for i in 0 to 3 loop
				cnt_tot (i)			<= std_logic_vector(to_unsigned(to_integer(unsigned (cnt_tot (i))) + 1, 64));
				if (cnt_tot(i) = x"FFFFFFFFFFFFFFFF") then
					cnt_tot (i)			<= (others => '0');
					cnt_hit	(i)			<= (others => '0');
				elsif (data_out_i(i) /= x"1e00000000000000") then
					cnt_hit (i)			<= std_logic_vector(to_unsigned(to_integer(unsigned (cnt_hit (i))) + 1, 64));
				end if;
			end loop;
		end if;
	end process occupancy_cnt;
	
end Behavioral;
