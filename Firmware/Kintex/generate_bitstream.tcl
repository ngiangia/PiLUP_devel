#FIRST TEST VERSION - TO BE REVIEWED!!!!!!!


# SETUP project
set N_PROC [exec nproc --all]
puts "Please select Project to build. Currently supported: \"template\", \"Protocol_Converter\" and \"Data_Generator\""
set appl_name [gets stdin]
set prj_name "PiLUP_Kintex_${appl_name}"
set root_dir [file dirname [info script]]
set prj_dir "${root_dir}/${prj_name}"

source generate_datetimehash_pkg.tcl

if {[file exists  ${prj_dir}/run] } {} else {exec mkdir ${prj_dir}/run}
open_project ${prj_dir}/${prj_name}.xpr
current_project ${prj_name}
reset_run synth_1
launch_runs impl_1 -to_step write_bitstream -jobs ${N_PROC} -dir ${prj_dir}/run
